import { BUBBLE_BOARD_EVENT } from '../Enum/BubbleBoard';
import { SHOOTER_EVENT } from '../Enum/BubbleShooter';
import { MASCOT_STATE } from '../Enum/Mascot';
import { SAFETY_LINE_STATE } from '../Enum/SafetyLine';
import { SCENE_KEY } from '../Enum/Scene';
import { STAGE_EVENT } from '../Library/Enum/Stage';
import AnimationManager from '../Library/Manager/AnimationManager';
import AssetManager from '../Library/Manager/AssetManager';
import ColorGenerator from '../Object/ColorGenerator';
import BubbleBoard from '../Object/Container/BubbleBoard';
import BubbleShooter from '../Object/Container/BubbleShooter';
import GameplayControl from '../Object/Container/GameplayControl';
import Mascot from '../Object/Container/Mascot';
import SafetyLine from '../Object/Container/SafetyLine';
import Trajectory from '../Object/Container/Trajectory';
import BackgroundImage from '../Object/Image/BackgroundImage';
import ShootPredictor from '../Object/ShootPredictor';
import Scene from '../Library/Scene/Scene';

export default class GameplayScene extends Scene {
  private background: BackgroundImage;

  constructor(assetManager: AssetManager, animationManager: AnimationManager) {
    super(SCENE_KEY.GAMEPLAY, assetManager, animationManager);
  }

  public start() {
    console.log('Gameplay Scene');
    this.background = new BackgroundImage(
      this,
      0, 
      0, 
      2, 
      this.stage.stageWidth, 
      this.stage.stageHeight
    );
    
    const safetyLine = new SafetyLine(this, this.stage.stageWidth * 0.5, this.stage.stageHeight * 0.7);
    const mascot = new Mascot(this, this.stage.stageWidth * 0.5, this.stage.stageHeight * 0.78);
    const colorGenerator = new ColorGenerator();
    const bubbleBoard = new BubbleBoard(this, 0, 0, colorGenerator);
    const bubbleShooter = new BubbleShooter(this, this.stage.stageWidth * 0.5, this.stage.stageHeight * 0.8, colorGenerator);
    const trajectory = new Trajectory(this);
    const shootPredictor = new ShootPredictor(
      this, 
      { x: this.stage.stageWidth * 0.5, y: this.stage.stageHeight * 0.8 }, 
      bubbleBoard
    );
    const gameplayControl = new GameplayControl(
      this, 
      bubbleBoard, 
      shootPredictor, 
      trajectory,
      bubbleShooter,
      this.stage.stageHeight * 0.8,
    );

    bubbleBoard.addEventListener(BUBBLE_BOARD_EVENT.BOARD_IN_DANGER, () => {
      safetyLine.setState(SAFETY_LINE_STATE.DANGER);
      mascot.setState(MASCOT_STATE.PANIC);
    }, this);
    
    bubbleBoard.addEventListener(BUBBLE_BOARD_EVENT.BOARD_IN_SAFETY, () => {
      safetyLine.setState(SAFETY_LINE_STATE.SAFE);
      mascot.setState(MASCOT_STATE.IDLE);
    }, this);

    bubbleShooter.addEventListener(SHOOTER_EVENT.SHOOT, (eventData: egret.Event) => {
      const { directionX, directionY } = eventData.data;
      mascot.playShootAnimation(directionX, directionY);
    }, this);

    bubbleBoard.once(BUBBLE_BOARD_EVENT.HIT_LAST_ROW, () => {
      this.stage.dispatchEvent(new egret.Event(STAGE_EVENT.GO_TO_SCENE, false, false, { sceneKey: SCENE_KEY.GAMEPLAY }));
    }, this);

    bubbleBoard.once(BUBBLE_BOARD_EVENT.CLEAR_ALL_BUBBLES, () => {
      this.stage.dispatchEvent(new egret.Event(STAGE_EVENT.GO_TO_SCENE, false, false, { sceneKey: SCENE_KEY.GAMEPLAY }));
    }, this);
  }

  public stop() {

  }
}