import { getAssetConfigs } from '../Config/AssetConfig';
import { LOADING_BAR_EVENT } from '../Library/Enum/LoadingBar';
import { SCENE_KEY } from '../Enum/Scene';
import { STAGE_EVENT } from '../Library/Enum/Stage';
import { LoadingUI } from '../Library/Component/LoadingUI';
import Scene from '../Library/Scene/Scene';
import BackgroundImage from '../Object/Image/BackgroundImage';
import { getCampaignDataV2 } from '../Service/CampaignServices';
import AssetManager from '../Library/Manager/AssetManager';
import { ASSET_KEY } from '../Enum/Asset';
import { getSpritesheetConfigs } from '../Config/SpritesheetConfig';
import { getAtlasCollection } from '../Config/AtlasConfig';
import AnimationManager from '../Library/Manager/AnimationManager';

export default class PreloadScene extends Scene {
  private loadingUI: LoadingUI;

  private background: BackgroundImage;

  public constructor(assetManager: AssetManager, animationManager: AnimationManager) {
    super(SCENE_KEY.PRELOAD, assetManager, animationManager);
  }

  public start() {
    this.background = new BackgroundImage(
      this, 
      0,
      0, 
      1, 
      this.stage.stageWidth, 
      this.stage.stageHeight
    );
    this.loadingUI = new LoadingUI(this);
    this.loadingUI.once(LOADING_BAR_EVENT.COMPLETE, () => {
      console.log('Asset load complete!');

      this.assetManager.registerSpritesheets(getSpritesheetConfigs());
      console.log('Spritesheet load complete!');

      this.assetManager.registerSpritesheetsFromAtlasCollection(getAtlasCollection());
      console.log('Spritesheet load from atlas complete!');

      setTimeout(() => {
        this.stage.dispatchEvent(new egret.Event(STAGE_EVENT.GO_TO_SCENE, false, false, { sceneKey: SCENE_KEY.GAMEPLAY }));
      }, 500);
    }, this);

    this.loadResource();
  }

  public stop() {

  }

  private async loadResource() {
    const campaignData = await getCampaignDataV2('7029e132c4edc0e8'); // TO-DO: replace with actual campaign id

    const assets = getAssetConfigs(campaignData.config);

    this.assetManager.loadAssets(assets, ASSET_KEY.ASSET_GROUP, 0, this.loadingUI);
  }
}