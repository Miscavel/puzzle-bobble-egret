export interface CampaignConfigV2 {
  image_100: string;
  image_129: string;
  image_168: string;

  // Mascot
  image_51: string;
  image_54: string;
  image_53: string;
  image_58: string;
}

export interface CampaignDataV2 {
  config: CampaignConfigV2;
}

export interface CampaignResponseV2 {
  data: CampaignDataV2;
}