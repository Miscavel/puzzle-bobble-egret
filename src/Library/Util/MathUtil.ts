import { Vector2 } from '../Interface/Vector';

export function getDistanceBetween(x1: number, y1: number, x2: number, y2: number) {
  const diffX = x2 - x1;
  const diffY = y2 - y1;
  const distance = Math.sqrt(Math.pow(diffX, 2.0) + Math.pow(diffY, 2.0));

  return distance;
}

export function getDirectionBetween(x1: number, y1: number, x2: number, y2: number) {
  const diffX = x2 - x1;
  const diffY = y2 - y1;
  const length = this.getDistanceBetween(x1, y1, x2, y2);

  return {
    x: diffX / length,
    y: diffY / length, 
  } as Vector2;
}

export function clamp(value: number, min: number, max: number) {
  return Math.min(
    Math.max(value, min),
    max
  );
}

export function sign(value: number) {
  return value / Math.abs(value);
}

/**
 * Returns the extend of overlap between 2 circles with known center and radius
 * @param x1 x of circle1
 * @param y1 y of circle1
 * @param radius1 radius of circle1
 * @param x2 x of circle2
 * @param y2 y of circle2
 * @param radius2 radius of circle2
 * @returns number
 */
export function getOverlap(
  x1: number,
  y1: number,
  radius1: number,
  x2: number,
  y2: number,
  radius2: number,
) {
  const distance = this.getDistanceBetween(x1, y1, x2, y2);

  const diff = (radius1 + radius2) - distance;

  return diff;
}