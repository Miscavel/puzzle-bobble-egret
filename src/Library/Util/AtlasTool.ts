import Scene from '../Scene/Scene';
import { ATLAS_ZERO_PAD } from './Constants';

/**
 * Returns the atlas animation frames for the given texture (e.g. from CampaignKey)
 * @param scene Phaser.Scene
 * @param textureKey textureKey from CampaignKey
 * @param start animation first index (usually 0)
 * @param end animation last index
 * @returns AnimationFrame[]
 */
export function getAtlasAnimationFrames(
    scene: Scene,
    textureKey: string, 
    start: number, 
    end: number
) {
  const key = scene.assetManager.getAtlasKey(textureKey, start);
  const prefix = scene.assetManager.getAtlasFrame(textureKey);

  return scene.animationManager.generateFrameNames(
    key,
    prefix,
    start,
    end,
    ATLAS_ZERO_PAD
  );
}