export const DEG_TO_RAD = (22 / 7) / 180;

/**
 * zeroPad used for the atlas' animation.
 * 
 * zeroPad = 3 means the number is written in a form where 0 is expressed as 000.
 * 
 * A bubble with 3 frames of animation will have ['bubble000', 'bubble001', 'bubble002'] as its frame keys.
 */
export const ATLAS_ZERO_PAD = 3;