import { SCENE_EVENT } from '../Enum/Scene';
import AnimationManager from '../Manager/AnimationManager';
import AssetManager from '../Manager/AssetManager';

export default abstract class Scene extends egret.DisplayObjectContainer {
  constructor(
    public readonly key: string,
    public readonly assetManager: AssetManager,
    public readonly animationManager: AnimationManager,
  ) {
    super();
    this.sortableChildren = true;
  }

  abstract start(): void;

  abstract stop(): void;

  public onStart() {
    this.start();
  }

  public onStop() {
    this.stop();
    egret.Tween.removeAllTweens();
    this.removeChildren();
  }

  /**
   * Registers an event listener
   * @param eventName name of the event to listen to
   * @param callback callback to be executed on event emit
   * @param context optional context for callback
   * @param once whether to be executed only once (defaults to false)
   * @param unregisterOnSceneShutdown whether to unregister the listener on scene shutdown
   * (defaults to true)
   */
  public registerEventListener(
    eventName: string,
    callback: Function,
    context: unknown = undefined,
    once = false,
    unregisterOnSceneShutdown = true,
  ) {
    if (once) {
      this.once(eventName, callback, context);
    } else {
      this.addEventListener(eventName, callback, context);
    }

    if (unregisterOnSceneShutdown) {
      this.unregisterEventListenerOnSceneShutdown(
        eventName,
        callback,
        context,
      );
    }
  }

  public unregisterEventListenerOnSceneShutdown(
    eventName: string,
    callback: Function,
    context: unknown = undefined,
  ) {
    this.once(
      SCENE_EVENT.SHUTDOWN, 
      () => {
        this.unregisterEventListener(eventName, callback, context);
      }, 
      this,
    );
  }

  public unregisterEventListener(
    eventName: string,
    callback: Function,
    context: unknown = undefined,
  ) {
    this.removeEventListener(eventName, callback, context);
  }
}