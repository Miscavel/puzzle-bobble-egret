import { FrameKey } from '../Type/FrameKey';

export interface AnimationFrame {
  textureKey: string;
  frameKey?: FrameKey;
}

export interface Animation {
  key: string;
  frames: Array<AnimationFrame>;
  frameRate: number;
  yoyo?: boolean;
  repeat?: number;
  reverseX?: boolean;
  reverseY?: boolean; 
}