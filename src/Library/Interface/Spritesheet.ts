export interface SpritesheetConfig {
  textureSource: string;
  frameWidth: number;
  frameHeight: number;
  padding: number;
}