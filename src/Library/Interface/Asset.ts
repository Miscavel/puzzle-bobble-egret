import { ASSET_TYPE } from '../Enum/Asset';

export interface AssetConfig {
  type: ASSET_TYPE;
  name: string;
  url: string;
}