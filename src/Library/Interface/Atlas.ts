export interface AtlasCollection {
  [key: string]: AtlasConfig
}

export interface AtlasConfig {
  frames: {
    [key: string]: AtlasFrameData
  }
}

export interface AtlasFrameData {
  frame: {
    x: number;
    y: number;
    w: number;
    h: number;
  }
}