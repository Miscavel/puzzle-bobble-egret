import { Animation, AnimationFrame } from '../Interface/Animation';
import { getZeroPaddedNum } from '../Util/StringUtil';

export default class AnimationManager {
  private animations: { [key: string]: Animation } = {};

  public registerAnimation(animation: Animation) {
    const { key } = animation;

    if (this.animations[key]) return;

    this.animations[key] = {
      yoyo: false,
      repeat: 0,
      reverseX: false,
      reverseY: false,
      ...animation
    };
  }

  public getAnimation(key: string) {
    return this.animations[key];
  }

  public generateFrameNumbers(key: string, start: number, end: number) {
    const frames = new Array<AnimationFrame>();
    for (let i = start; i <= end; i++) {
      frames.push({
        textureKey: key,
        frameKey: i
      });
    }
    return frames;
  }

  public generateFrameNames(key: string, prefix: string, start: number, end: number, zeroPad: number) {
    const frames = new Array<AnimationFrame>();
    for (let i = start; i <= end; i++) {
      const numString = getZeroPaddedNum(i, zeroPad);

      frames.push({
        textureKey: key,
        frameKey: `${prefix}${numString}`,
      });
    }
    return frames;
  }
}