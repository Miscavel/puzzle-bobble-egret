import { AssetConfig } from '../Interface/Asset';
import { AtlasCollection, AtlasConfig, AtlasFrameData } from '../../Library/Interface/Atlas';
import { SpritesheetConfig } from '../Interface/Spritesheet';
import { LoadingUI } from '../Component/LoadingUI';
import { FrameKey } from '../Type/FrameKey';
import { getEntries } from '../Util/ObjectUtil';
import { getZeroPaddedNum } from '../Util/StringUtil';
import { ATLAS_ZERO_PAD } from '../Util/Constants';

export default class AssetManager {
  private spritesheets: { [key: string]: egret.SpriteSheet } = {}; 

  private atlasCollection: AtlasCollection = {};

  constructor() {
    // Bypass CORS
    egret.ImageLoader.crossOrigin = "anonymous";
    
    // Trigger instance creation
    RES.getVersionController(); 
    
    // Initialize config
    RES.config.config = {
      alias: {}, 
      groups: {}, 
      resourceRoot: "",
      mergeSelector: null,
      fileSystem:  {
        fsData: {},
        getFile: function (filename) {
            return this.fsData[filename];
        },
        addFile: function (data) {
            if (!data.type)
                data.type = "";
            if (data.root == undefined) {
                data.root = "";
            }
            this.fsData[data.name] = data;
        },
        profile: function () {
            console.log(this.fsData);
        },
        removeFile: function (filename) {
            delete this.fsData[filename];
        }
      } as unknown as RES.FileSystem,
      loadGroup: []
    };
  }

  /**
   * Loads assets from urls provided through the array of AssetConfig
   * @param assetConfigs Array<AssetConfig>
   * @param groupName string
   * @param priority number
   * @param loadingUI LoadingUI
   */
  public loadAssets(assetConfigs: Array<AssetConfig>, groupName: string, priority?: number, loadingUI?: LoadingUI) {
    const assetKeys = assetConfigs.map((assetConfig) => {
      return assetConfig.name;
    });

    RES.config.createGroup(groupName, assetKeys);
    assetConfigs.forEach((assetConfig) => {
      RES.config.addResourceData(assetConfig);
    });

    RES.loadGroup(groupName, priority, loadingUI);
  }

  /**
   * Returns texture loaded in RES or spritesheets
   * @param textureKey string
   * @returns egret.Texture | undefined | null
   */
  public getTexture(textureKey: string, frameKey?: FrameKey): egret.Texture | undefined | null {
    if (frameKey !== undefined) {
      return this.getTextureFromSpritesheets(textureKey, frameKey);
    }
    return RES.getRes(textureKey);
  }

  /**
   * Returns texture loaded in spritesheets
   * @param textureKey string
   * @param frameKey number
   * @returns egret.Texture | undefined | null
   */
  private getTextureFromSpritesheets(textureKey: string, frameKey: FrameKey): egret.Texture | undefined {
    return this.spritesheets[textureKey]?.getTexture(String(frameKey));
  }

  /**
   * Registers array of SpritesheetConfig
   * @param spritesheetConfigs Array<SpritesheetConfig>
   */
  public registerSpritesheets(spritesheetConfigs: Array<SpritesheetConfig>) {
    spritesheetConfigs.forEach((spritesheetConfig) => {
      this.registerSpritesheet(spritesheetConfig);
    });
  }

  /**
   * Registers a SpritesheetConfig and generates the texture on each frame
   * @param spritesheetConfig SpritesheetConfig
   * @returns void
   */
  public registerSpritesheet(spritesheetConfig: SpritesheetConfig) {
    const { textureSource, frameWidth, frameHeight, padding } = spritesheetConfig;

    const spritesheetTexture = this.getTexture(textureSource);

    // If spritesheet of the same key exists or spritesheet texture does not exist, return immediately
    if (this.spritesheets[textureSource] || !spritesheetTexture) return;

    const spritesheet = new egret.SpriteSheet(spritesheetTexture);
    const { textureWidth, textureHeight } = spritesheetTexture;
    
    let frameKey = 0;
    for (let row = 0; row < textureHeight; row += (frameHeight + padding)) {
      for (let col = 0; col < textureWidth; col += (frameWidth + padding)) {
        spritesheet.createTexture(String(frameKey++), col, row, frameWidth, frameHeight);
      }
    }

    this.spritesheets[textureSource] = spritesheet;
  }

  /**
   * Registers spritesheets from the given AtlasCollection
   * @param atlasCollection AtlasCollection
   */
  public registerSpritesheetsFromAtlasCollection(atlasCollection: AtlasCollection) {
    this.atlasCollection = { ...atlasCollection, ...this.atlasCollection };
    getEntries(atlasCollection).forEach(([key, atlasConfig]) => {
      this.registerSpritesheetFromAtlasConfig(key as string, atlasConfig as AtlasConfig);
    });
  }

  /**
   * Registers a spritesheet from the given textureSource and AtlasConfig
   * @param textureSource string
   * @param atlasConfig AtlasConfig
   * @returns void
   */
  public registerSpritesheetFromAtlasConfig(textureSource: string, atlasConfig: AtlasConfig) {
    const spritesheetTexture = this.getTexture(textureSource);

    if (this.spritesheets[textureSource] || !spritesheetTexture) return;

    const spritesheet = new egret.SpriteSheet(spritesheetTexture);

    getEntries(atlasConfig.frames).forEach(([atlasFrameKey, atlasFrameData]) => {
      const { x, y, w, h } = (atlasFrameData as AtlasFrameData).frame;
      spritesheet.createTexture(atlasFrameKey as string, x, y, w, h);
    });

    this.spritesheets[textureSource] = spritesheet;
  }

  /**
   * Returns the atlas key for the given texture (e.g. from CampaignKey)
   * @param textureKey textureKey from CampaignKey
   * @param index index for animated sprite (e.g. number 0 for 'gameplay_fish_destroy000') or color based sprite (e.g. string 'Red' in 'fishRed')
   * @returns atlasKey
   */
  public getAtlasKey(textureKey: string, index?: number | string) {
    const atlasFrame = this.getAtlasFrame(textureKey, index);

    return getEntries(this.atlasCollection).reduce((atlasKey, [key, config]) => {
      const frames = (
          (config as AtlasConfig).frames ?? { 'dummy': undefined }
      ) as { [key: string]: object };
      
      if (frames[atlasFrame] !== undefined) {
          return atlasKey ?? key as string;
      }

      return atlasKey;
    }, undefined as unknown as string);
  }

  /**
   * Returns the atlas frame for the given texture (e.g. from CampaignKey)
   * @param textureKey textureKey from CampaignKey
   * @param index index for animated sprite (e.g. number 0 for 'gameplay_fish_destroy000') or color based sprite (e.g. string 'Red' in 'fishRed')
   * @returns atlasFrame
   */
  public getAtlasFrame(textureKey: string, index?: number | string) {
    switch(typeof index) {
      case 'string': {
          return `${textureKey}${index}`;
      }

      case 'number': {
          return `${textureKey}${getZeroPaddedNum(index, ATLAS_ZERO_PAD)}`;
      }

      default: {
          return textureKey;
      }
    }
  }
}