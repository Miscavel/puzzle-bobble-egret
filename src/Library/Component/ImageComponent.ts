import { IMAGE_COMPONENT_EVENT } from '../Enum/ImageComponent';
import Scene from '../Scene/Scene';
import { SCENE_EVENT } from '../Enum/Scene';
import { FrameKey } from '../Type/FrameKey';
import Animator from './Animator';
import { ANIMATION_EVENT } from '../Enum/Animation';
import { AnimationFrame } from '../Interface/Animation';

export default abstract class ImageComponent extends egret.Bitmap {
  protected animator: Animator;

  constructor(
    protected scene: Scene,
    private textureKey: string,
    private frameKey?: FrameKey,
    x?: number,
    y?: number,
    width?: number,
    height?: number,
  ) {
    super(scene.assetManager.getTexture(textureKey, frameKey));
    scene.addChild(this);

    this.setTransformProperties(x, y, width, height);
    if (this.isTextureReady()) {
      this.setTransformProperties(x, y, width, height);
    } else {
      scene.registerEventListener(SCENE_EVENT.UPDATE, this.reloadTextureOnceReady, this);
      this.once(IMAGE_COMPONENT_EVENT.READY, () => {
        this.setTransformProperties(x, y, width, height);
      }, this);
    }

    this.animator = new Animator(scene);
    this.animator.addEventListener(ANIMATION_EVENT.UPDATE, this.animatorUpdate, this);
    this.animator.addEventListener(ANIMATION_EVENT.COMPLETE, this.animatorComplete, this);
  }

  private reloadTextureOnceReady() {
    if (this.isTextureReady()) {
      this.reloadTexture();
      this.dispatchEvent(new egret.Event(IMAGE_COMPONENT_EVENT.READY));
      this.scene.unregisterEventListener(SCENE_EVENT.UPDATE, this.reloadTextureOnceReady, this);
    }
  }

  private getAssignedTexture() {
    return this.scene.assetManager.getTexture(this.textureKey, this.frameKey);
  }

  private isTextureReady() {
    const texture = this.getAssignedTexture();
    return (texture !== null && texture !== undefined);
  }

  private reloadTexture() {
    this.texture = this.getAssignedTexture();
  }

  private setTransformProperties(
    x?: number, 
    y?: number, 
    width?: number, 
    height?: number
  ) {
    this.x = x ?? this.x;
    this.y = y ?? this.y;
    this.width = width ?? this.width;
    this.height = height ?? this.height;
  }

  public updateTexture(textureKey: string, frameKey?: FrameKey, reverseX?: boolean, reverseY?: boolean) {
    this.textureKey = textureKey;
    this.frameKey = frameKey;
    this.scaleX = reverseX ? -1 : 1;
    this.scaleY = reverseY ? -1 : 1;
    this.reloadTexture();
  }

  private animatorUpdate(eventData: egret.Event) {
    const { frameToRender, reverseX, reverseY } = eventData.data;
    const { textureKey, frameKey } = frameToRender as AnimationFrame;

    this.updateTexture(textureKey, frameKey, reverseX, reverseY);
  }

  private animatorComplete() {
    this.dispatchEvent(new egret.Event(ANIMATION_EVENT.COMPLETE));
  }
}