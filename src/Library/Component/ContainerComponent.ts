import Scene from '../Scene/Scene';

export default class ContainerComponent extends egret.DisplayObjectContainer {
  constructor(
    protected scene: Scene,
    x: number,
    y: number,
  ) {
    super();
    this.setPosition(x, y);
  }

  public setPosition(x: number, y: number) {
    this.x = x;
    this.y = y;
  }
}