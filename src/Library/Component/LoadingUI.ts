import { LOADING_BAR_EVENT } from '../Enum/LoadingBar';
import LoadingBar from './LoadingBar';
import Scene from '../Scene/Scene';
import ContainerComponent from './ContainerComponent';

export class LoadingUI extends ContainerComponent implements RES.PromiseTaskReporter {
    private loadingBar: LoadingBar;

    public constructor(scene: Scene) {
        super(scene, 0, 0);
        scene.addChild(this);
        this.createView();
    }
    
    private createView(): void {
        const { stageWidth, stageHeight } = this.scene.stage;

        this.addChild(
            this.loadingBar = new LoadingBar(
                this.scene,
                stageWidth / 2,
                stageHeight / 2,
                stageWidth * 0.8,
                stageHeight * 0.05
            )
        );

        this.loadingBar.addEventListener(LOADING_BAR_EVENT.COMPLETE, () => {
            this.dispatchEvent(new egret.Event(LOADING_BAR_EVENT.COMPLETE));
        }, this);
    }

    public onProgress(current: number, total: number): void {
        this.loadingBar.updateProgress((current / total) * 100);
    }
}
