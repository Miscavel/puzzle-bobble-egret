export enum STAGE_EVENT {
  UPDATE = 'update',
  GO_TO_SCENE = 'go_to_scene',
}