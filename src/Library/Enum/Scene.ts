export enum SCENE_INSTANCE_STATE {
  RUNNING = 'running',
  STOPPED = 'stopped',
}

export enum SCENE_EVENT {
  UPDATE = 'update',
  SHUTDOWN = 'shutdown',
}