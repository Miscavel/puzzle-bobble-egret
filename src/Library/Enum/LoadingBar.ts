export enum LOADING_BAR_EVENT {
  UPDATE = 'update',
  COMPLETE = 'complete',
}