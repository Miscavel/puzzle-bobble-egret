import { ANIMATION_EVENT } from '../../Library/Enum/Animation';
import { ColorEnum } from '../../Enum/Color';
import Scene from '../../Library/Scene/Scene';
import { setOrigin } from '../../Library/Util/DisplayObjectUtil';
import BubbleImage from '../Image/BubbleImage';
import Bubble from './Bubble';
import ContainerComponent from '../../Library/Component/ContainerComponent';

export default class BubbleSlot extends ContainerComponent {
  private bubble?: Bubble;

  private hintImage: BubbleImage;

  private neighbors = new Array<BubbleSlot>();

  constructor(
    scene: Scene, 
    x: number, 
    y: number,
    public readonly row: number,
    public readonly col: number,
    private slotWidth: number,
    private slotHeight: number,
    bubbleColor?: ColorEnum
  ) {
    super(scene, x, y);

    this.spawnBubbleOfColor(bubbleColor);
    this.createHintImage();
    this.hideHintImage();
  }

  public spawnBubbleOfColor(color?: ColorEnum) {
    if (color === undefined || this.bubble) return;

    const { scene, slotWidth, slotHeight } = this;

    this.addChild(
      setOrigin(
        this.bubble = new Bubble(
          scene, 
          0, 
          0, 
          slotWidth * 1.15, 
          slotHeight * 1.15,
          color
        ),
        0.5,
        0.5
      )
    );
  }

  private createHintImage() {
    const { scene, slotWidth, slotHeight } = this;
    
    this.addChild(
      setOrigin(
        this.hintImage = new BubbleImage(
          scene, 
          0, 
          0, 
          ColorEnum.BLUE, 
          slotWidth * 1.15, 
          slotHeight * 1.15,
        ),
        0.5,
        0.5
      )
    );

    this.hintImage.alpha = 0.5;
  }

  public showHintImage(color: ColorEnum) {
    this.hintImage.updateColor(color);
    this.hintImage.visible = true;
  }

  public hideHintImage() {
    this.hintImage.visible = false;
  }
  
  public registerNeighbor(neighbor?: BubbleSlot) {
    const { neighbors } = this;

    if (!neighbor || neighbors.indexOf(neighbor) !== -1) return;

    neighbors.push(neighbor);
  }

  public getNeighbors() {
    return this.neighbors;
  }

  public matchSlot(bubbleSlot: BubbleSlot) {
    return this.bubble?.getColor() === bubbleSlot.bubble?.getColor();
  }

  public pop(delay = 0) {
    const { bubble } = this;

    if (!bubble) return;

    this.removeBubble();
    this.animatePop(bubble, delay);
  }

  public drop() {
    const { bubble } = this;

    if (!bubble) return;

    this.removeBubble();
    this.animateDrop(bubble);
  }

  private removeBubble() {
    this.removeChild(this.bubble);
    this.bubble = undefined;
  }

  private animatePop(bubble: Bubble, delay = 0) {
    const { scene } = this;

    const { x, y } = this.localToGlobal(0, 0);
    scene.addChild(bubble);
    bubble.setPosition(x, y);

    bubble.pop(delay);
    bubble.getImage().once(ANIMATION_EVENT.COMPLETE, () => {
      bubble.parent?.removeChild(bubble);
    }, this);
  }

  private animateDrop(bubble: Bubble) {
    const { scene } = this;
    const { stageWidth, stageHeight } = this.scene.stage;

    const { x, y } = this.localToGlobal(0, 0);
    scene.addChild(bubble);
    bubble.setPosition(x, y);

    const floorY = stageHeight * 0.8;
    const bounceHeight = (floorY - y) * 0.2;
    const bounceWidth = (x - (stageWidth * 0.5)); 

    const dropTween = egret.Tween.get(bubble);
    dropTween.to({ 'y': floorY }, 500, egret.Ease.sineIn);
    dropTween.to({ 'y': floorY - bounceHeight, 'x': x + bounceWidth, 'alpha': 0 }, 300, egret.Ease.sineOut);
    dropTween.call(() => {
      bubble.parent?.removeChild(bubble);
    });
  }

  public getBubble() {
    return this.bubble;
  }
}