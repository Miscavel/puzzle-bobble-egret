import { SHOOTER_EVENT, SHOOTER_STATE } from '../../Enum/BubbleShooter';
import { SCENE_EVENT } from '../../Library/Enum/Scene';
import { Vector2 } from '../../Library/Interface/Vector';
import Scene from '../../Library/Scene/Scene';
import { setOrigin } from '../../Library/Util/DisplayObjectUtil';
import ColorGenerator from '../ColorGenerator';
import Bubble from './Bubble';
import ContainerComponent from '../../Library/Component/ContainerComponent';

export default class BubbleShooter extends ContainerComponent {
  private readonly BUBBLE_SHOOT_SPEED = 30;

  private bubble?: Bubble;

  private state: SHOOTER_STATE = SHOOTER_STATE.READY;
  
  constructor(
    scene: Scene, 
    x: number, 
    y: number,
    private colorGenerator: ColorGenerator
  ) {
    super(scene, x, y);
    scene.addChild(this);

    this.addChild(
      setOrigin(
        this.bubble = new Bubble(scene, 0, 0, this.getBubbleDiameter(), this.getBubbleDiameter(), colorGenerator.getRandomColor()),
        0.5,
        0.5
      )
    );
  }

  private reloadBubble() {
    this.bubble.x = 0;
    this.bubble.y = 0;
    this.bubble.setColor(this.colorGenerator.getRandomColor());

    this.setState(SHOOTER_STATE.READY);
  }

  // TO-DO: extract from BubbleBoard
  private getBubbleDiameter() {
    return (this.scene.stage.stageWidth / 9) * 1.15;
  }

  // TO-DO: extract from BubbleBoard
  private getBubbleRadius() {
    return this.getBubbleDiameter() * 0.5;
  }

  private getLeftBounds() {
    return this.getBubbleRadius() - this.x;
  }

  private getRightBounds() {
    return this.scene.stage.stageWidth - this.getBubbleRadius() - this.x;
  }

  private getBubbleWorldY() {
    return this.bubble.y + this.y;
  }

  public isReady() {
    return this.state === SHOOTER_STATE.READY;
  }

  private setState(state: SHOOTER_STATE) {
    this.state = state;
  }

  public getCurrentBubbleColor() {
    return this.bubble?.getColor();
  }

  public launchBubble(
    directionX: number, 
    directionY: number,
    target: Vector2
  ) {
    const { scene, BUBBLE_SHOOT_SPEED } = this;

    if (!this.isReady()) return;

    this.setState(SHOOTER_STATE.SHOOTING);

    this.dispatchEvent(new egret.Event(SHOOTER_EVENT.SHOOT, false, false, { directionX, directionY }));

    let directionXMultiplier = 1;

    const bubbleLaunchUpdate = () => {
      this.bubble.x += directionX * directionXMultiplier * BUBBLE_SHOOT_SPEED;
      this.bubble.y += directionY * BUBBLE_SHOOT_SPEED;

      if (this.bubble.x <= this.getLeftBounds()) {
        this.bubble.x = this.getLeftBounds();
        directionXMultiplier *= -1;
      } else if (this.bubble.x >= this.getRightBounds()) {
        this.bubble.x = this.getRightBounds();
        directionXMultiplier *= -1;
      }

      if (this.getBubbleWorldY() <= target.y) {
        this.dispatchEvent(new egret.Event(SHOOTER_EVENT.SNAP, false, false, { color: this.bubble.getColor() }));
      }
    };

    scene.registerEventListener(SCENE_EVENT.UPDATE, bubbleLaunchUpdate, this);

    this.once(SHOOTER_EVENT.SNAP, () => {
      scene.unregisterEventListener(SCENE_EVENT.UPDATE, bubbleLaunchUpdate, this);
      this.reloadBubble();
    }, this);
  }
}