import { Vector2 } from '../../Library/Interface/Vector';
import Scene from '../../Library/Scene/Scene';
import ContainerComponent from '../../Library/Component/ContainerComponent';

export default class Trajectory extends ContainerComponent {
  private readonly DOT_COUNT = 200;

  private readonly DOT_COLOR = 0xff0000;

  private dots = new Array<egret.Shape>();
  
  constructor(scene: Scene) {
    super(scene, 0, 0);
    scene.addChild(this);

    this.generateDots();
    this.hideDots();
  }

  private generateDots() {
    const { DOT_COUNT } = this;

    this.clearDots();

    for (let i = 0; i < DOT_COUNT; i++) {
      this.addDot();
    }
  }

  private clearDots() {
    this.dots.forEach((dot) => {
      this.removeChild(dot);
    });

    this.dots = [];
  }

  private addDot() {
    const dot = this.createDot();
    this.dots.push(dot);
    this.addChild(dot);
  }

  private createDot() {
    const { DOT_COLOR } = this;

    const dot = new egret.Shape();
    dot.graphics.beginFill(DOT_COLOR);
    dot.graphics.drawCircle(0, 0, this.getDotRadius());
    dot.graphics.endFill();
    // TO-DO: why no need set origin?
    // setOrigin(dot, 0.5, 0.5); 

    return dot;
  }

  // TO-DO: something
  private getDotRadius() {
    return this.scene.stage.stageWidth / 80;
  }

  public updateDotsPosition(positions: Array<Vector2>) {
    this.dots.forEach((dot, index) => {
      if (index < positions.length) {
        const { x, y } = positions[index];
        dot.x = x;
        dot.y = y;

        dot.visible = true;
      } else {
        dot.visible = false;
      }
    });
  }

  public showDots() {
    this.visible = true;
  }

  public hideDots() {
    this.visible = false;
  }
}