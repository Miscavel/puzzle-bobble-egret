import { SHOOTER_EVENT } from '../../Enum/BubbleShooter';
import Scene from '../../Library/Scene/Scene';
import { getDirectionBetween, sign } from '../../Library/Util/MathUtil';
import ShootPredictor from '../ShootPredictor';
import BubbleBoard from './BubbleBoard';
import BubbleShooter from './BubbleShooter';
import BubbleSlot from './BubbleSlot';
import ContainerComponent from '../../Library/Component/ContainerComponent';
import Trajectory from './Trajectory';

export default class GameplayControl extends ContainerComponent {
  private inputArea: egret.Shape;

  private lastHintBubbleSlot: BubbleSlot;
  
  constructor(
    scene: Scene,
    private bubbleBoard: BubbleBoard,
    private shootPredictor: ShootPredictor,
    private trajectory: Trajectory,
    private bubbleShooter: BubbleShooter,
    private shootZoneYLimit: number,
  ) {
    super(scene, 0, 0);
    scene.addChild(this);

    this.addChild(
      this.inputArea = this.createInputArea()
    );

    this.inputArea.touchEnabled = true;
    this.inputArea.addEventListener(egret.TouchEvent.TOUCH_BEGIN, (touchEvent) => {
      this.updateTrajectory(touchEvent);
    }, this.inputArea);

    this.inputArea.addEventListener(egret.TouchEvent.TOUCH_END, () => {
      this.hideTrajectory();
    }, this.inputArea);

    this.inputArea.addEventListener(egret.TouchEvent.TOUCH_TAP, (touchEvent) => {
      console.log('Launch');
      this.launch(touchEvent);
    }, this.inputArea);

    this.inputArea.addEventListener(egret.TouchEvent.TOUCH_RELEASE_OUTSIDE, () => {
      console.log('Cancel');
      this.hideTrajectory();
    }, this.inputArea);

    this.inputArea.addEventListener(egret.TouchEvent.TOUCH_MOVE, (touchEvent) => {
      this.updateTrajectory(touchEvent);
    }, this.inputArea);
  }

  private createInputArea() {
    const { stageWidth, stageHeight } = this.scene.stage;

    const inputArea = new egret.Shape();
    inputArea.graphics.beginFill(0xff0000, 0);
    inputArea.graphics.drawRect(0, 0, stageWidth, stageHeight);
    inputArea.graphics.endFill();

    return inputArea;
  }

  private getTouchData(touchEvent: egret.TouchEvent) {
    const { shootPredictor } = this;

    const { x, y } = getDirectionBetween(
      shootPredictor.shootAnchor.x,
      shootPredictor.shootAnchor.y,
      touchEvent.stageX, 
      touchEvent.stageY
    );

    const angle = Math.max(Math.abs(Math.atan(y / x)), 0.5);

    const directionX = sign(x) * Math.abs(Math.cos(angle));
    const directionY = -Math.abs(Math.sin(angle));

    if (sign(y) !== sign(directionY)) return;

    const { points: predictions, collidedBubbleSlot } = shootPredictor.getPrediction(directionX, directionY);

    return {
      directionX,
      directionY,
      predictions,
      collidedBubbleSlot,
    };
  }

  private updateTrajectory(touchEvent: egret.TouchEvent) {
    if (!this.isWithinPlayingField(touchEvent)) {
      this.hideTrajectory();
      return;
    }

    this.showTrajectory();

    const { trajectory, bubbleBoard, bubbleShooter } = this;
    const { predictions, collidedBubbleSlot } = this.getTouchData(touchEvent);
    trajectory.updateDotsPosition(predictions);

    const { x: landX, y: landY } = predictions.pop();
    this.lastHintBubbleSlot?.hideHintImage();
    this.lastHintBubbleSlot = bubbleBoard.getSnapBubbleSlot(landX, landY, collidedBubbleSlot);
    this.lastHintBubbleSlot?.showHintImage(bubbleShooter.getCurrentBubbleColor());
  }

  private showTrajectory() {
    this.trajectory.showDots();
  }

  private hideTrajectory() {
    this.trajectory.hideDots();
    this.lastHintBubbleSlot?.hideHintImage();
  }

  private isWithinPlayingField(touchEvent: egret.TouchEvent) {
    return touchEvent.stageY <= this.shootZoneYLimit;
  }

  private launch(touchEvent: egret.TouchEvent) {
    const { bubbleShooter, bubbleBoard } = this;
    const { directionX, directionY, predictions, collidedBubbleSlot } = this.getTouchData(touchEvent);
    
    if (!bubbleShooter.isReady()) return;

    const target = predictions.pop();
    bubbleShooter.launchBubble(directionX, directionY, target);
    bubbleShooter.once(SHOOTER_EVENT.SNAP, (eventData: egret.Event) => {
      bubbleBoard.snapBubbleAt(target.x, target.y, eventData.data.color, collidedBubbleSlot);
    }, this);
  }
}