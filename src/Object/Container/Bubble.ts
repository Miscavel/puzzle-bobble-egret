import { ColorEnum } from '../../Enum/Color';
import Scene from '../../Library/Scene/Scene';
import BubbleImage from '../Image/BubbleImage';
import ContainerComponent from '../../Library/Component/ContainerComponent';

export default class Bubble extends ContainerComponent {
  private image: BubbleImage;

  constructor(
    scene: Scene, 
    x: number, 
    y: number,
    private bubbleWidth: number,
    private bubbleHeight: number,
    private color: ColorEnum,
  ) {
    super(scene, x, y);
    this.addChild(
      this.image = new BubbleImage(scene, x, y, color, bubbleWidth, bubbleHeight)
    );
  }

  public getImage() {
    return this.image;
  }

  public getColor() {
    return this.color;
  }

  public setColor(color: ColorEnum) {
    this.color = color;
    this.image.updateColor(color);
  }

  public pop(delay = 0) {
    this.image.playPopAnimation(delay);
  }
}