import { Vector2 } from '../Library/Interface/Vector';
import Scene from '../Library/Scene/Scene';
import { getOverlap } from '../Library/Util/MathUtil';
import BubbleBoard from './Container/BubbleBoard';
import BubbleSlot from './Container/BubbleSlot';

export type ShootPredictorData = {
  overlap: number;
  bubbleSlot?: BubbleSlot;
}

export default class ShootPredictor {
  private step = 30;

  constructor(
    private scene: Scene,
    public readonly shootAnchor: Vector2,
    private bubbleBoard: BubbleBoard
  ) {
  }

  public getPrediction(directionX: number, directionY: number) {
    const { bubbleBoard } = this;
    const { x, y } = this.shootAnchor;
    const radius = this.getBubbleRadius();
    const points = new Array<Vector2>();
    let collidedBubbleSlot: BubbleSlot;

    let pointX = x;
    let pointY = y;

    let incrementX = directionX;
    let incrementY = directionY;

    while (pointY >= 0) {
      pointX += incrementX * this.step;
      pointY += incrementY * this.step;

      const incrementXMultiplier = this.getIncrementXMultiplier(pointX);
      pointX = this.clampPointX(pointX);

      const bubbleSlot = bubbleBoard.getBubbleSlotFromPosition(pointX, pointY);
      if (bubbleSlot) {
        const checkSlots = [ bubbleSlot, ...bubbleSlot.getNeighbors() ];
        const highestOverlap = checkSlots.reduce((result, checkSlot) => {
          const bubble = checkSlot.getBubble();
          
          if (bubble) {
            const { x: slotX, y: slotY } = checkSlot;
            const overlap = getOverlap(
              bubbleBoard.getInBoardX(pointX), 
              bubbleBoard.getInBoardY(pointY), 
              radius, 
              slotX, 
              slotY, 
              radius
            );

            if (overlap > result.overlap) {
              return { overlap, bubbleSlot: checkSlot };
            }
          }
          
          return result;
        }, { overlap: -1, bubbleSlot: undefined } as ShootPredictorData);

        if (highestOverlap.overlap >= 0) {
          points.push({ x: pointX, y: pointY });
          collidedBubbleSlot = highestOverlap.bubbleSlot;
          break;
        }
      }

      points.push({ x: pointX, y: pointY });
      incrementX *= incrementXMultiplier;
    }

    return { points, collidedBubbleSlot };
  }

  private getLeftBound() {
    return this.getBubbleRadius();
  }

  private getRightBound() {
    return this.scene.stage.stageWidth - this.getBubbleRadius();
  }

  private getIncrementXMultiplier(x: number) {
    if (
      x < this.getLeftBound() ||
      x > this.getRightBound()
    ) {
      return -1;
    }

    return 1;
  }

  // TO-DO: extract to util
  private clampPointX(x: number) {
    return Math.min(
      Math.max(x, this.getLeftBound()),
      this.getRightBound()
    );
  }

  // TO-DO: extract from BubbleBoard
  private getBubbleDiameter() {
    return (this.scene.stage.stageWidth / 9);
  }

  // TO-DO: extract from BubbleBoard
  private getBubbleRadius() {
    return this.getBubbleDiameter() * 0.5;
  }
}