import { ANIMATION_KEY } from '../../Enum/Animation';
import { ASSET_KEY } from '../../Enum/Asset';
import Scene from '../../Library/Scene/Scene';
import { getAtlasAnimationFrames } from '../../Library/Util/AtlasTool';
import ImageComponent from '../../Library/Component/ImageComponent';

export default class SafetyLineImage extends ImageComponent {
  constructor(
    scene: Scene,
    x: number, 
    y: number,
    width?: number, 
    height?: number
  ) {
    super(
      scene, 
      scene.assetManager.getAtlasKey(ASSET_KEY.GAMEPLAY_SAFETY_LINE),
      scene.assetManager.getAtlasFrame(ASSET_KEY.GAMEPLAY_SAFETY_LINE),
      x,
      y,
      width,
      height,
    );

    this.registerDangerAnimation();
  }

  private registerDangerAnimation() {
    const { scene } = this;
    const { animationManager } = scene;

    animationManager.registerAnimation({
      key: ANIMATION_KEY.SAFETY_LINE_DANGER,
      frames: getAtlasAnimationFrames(scene, ASSET_KEY.GAMEPLAY_SAFETY_LINE_DANGER, 0, 2),
      frameRate: 8,
      yoyo: true,
      repeat: -1
    });
  }

  public playDangerAnimation() {
    this.animator.play(ANIMATION_KEY.SAFETY_LINE_DANGER);
  }

  public stopDangerAnimation() {
    const { assetManager } = this.scene;
    
    this.animator.stop();
    this.updateTexture(
      assetManager.getAtlasKey(ASSET_KEY.GAMEPLAY_SAFETY_LINE), 
      assetManager.getAtlasFrame(ASSET_KEY.GAMEPLAY_SAFETY_LINE)
    );
  }
}