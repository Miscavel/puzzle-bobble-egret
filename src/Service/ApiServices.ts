export const defaultOrigin =
  window.location.origin.indexOf('shopee') === -1
    ? 'https://cors-anywhere-misc.herokuapp.com/https://play.shopee.co.id'
    : window.location.origin;
// TO-DO: remove dem cors-anywhere and revert to play.test

// Used for DRY (Don't Repeat Yourself) adding prefix at routes
const prefixRoutes = {
  v1: '/api/puzzle-bobble-be/v1',
  v2: '/api/puzzle-bobble-be/v2',
  v3: '/api/puzzle-bobble-be/v3',
  test: '/api/rendang/v1/puzzle-bobble/admin', // TO-DO: no process env?
};

/**
 * This interface extends from Request Init fetch API
 * add params as query like axios API
 */
 export interface RequestInitService extends RequestInit {
  params?: {
    [props: string]: string | number;
  };
}

/**
 * Used to store API routes, mock API routes
 * should be the same as real world/production api
 */
 const routes = {
  // V2
  campaignV2: (eventId: string) => `${prefixRoutes.v2}/events/${eventId}`,
};

/**
 *
 * @param origin This params receive custom origin to be used in fetch
 *
 * This function receive optional custom origin value
 * but if the value is not given it will use default origin value
 * currently used to change between mock api and real api for development
 */
 const getOrigin = (origin?: string) =>
 origin !== undefined ? origin : defaultOrigin;

/**
*
* @param init RequestService extend from RequestInit
*
* This function will return '' | '?query=value'
*/
const parseQuery = (init?: RequestInitService): string => {
 if (!init || !init.params) {
   return '';
 }

 const { params } = init;
 const keys = Object.keys(params);

 let query = '?';
 for (let i = 0; i < keys.length; i++) {
   const key = keys[i];
   const value = params[key];

   if (value !== undefined) {
     query += `${key}=${params[key]}${keys[i + 1] ? '&' : ''}`;
   }
 }

 return query;
};

// TO-DO AbortController error when published

/**
 *
 * @param route Routes for fetch, will be placed after origin value
 * @param init RequestService extend from RequestInit
 * @param origin Custom origin replace defaultOrigin
 *
 * Create fetch function with method GET with
 * x-tenant, x-user-id, x-device-id headers
 * work the same as normal fetch
 */
 const get = (
  route: string,
  init?: RequestInitService | undefined,
  origin?: string,
): Promise<Response> => {
  const { headers } = init || {};

  const fetchParam = fetch(`${getOrigin(origin)}${route}${parseQuery(init)}`, {
    ...init,
    method: 'GET',
    credentials: 'omit',
    headers: {
      ...headers,
      'x-tenant': 'id', // TO-DO: replace with actual values
      'x-user-id': '',
      'x-device-id': '',
      'origin': 'misc',
    },
  });
  return fetchParam;
};

const isOK = (res: Response) => res.status >= 200 && res.status < 300;

export default {
  routes,
  get,
  isOK
};