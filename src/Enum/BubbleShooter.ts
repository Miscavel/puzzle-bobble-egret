export enum SHOOTER_EVENT {
  SNAP = 'snap',
  SHOOT = 'shoot',
}

export enum SHOOTER_STATE {
  READY = 'ready',
  SHOOTING = 'shooting',
}