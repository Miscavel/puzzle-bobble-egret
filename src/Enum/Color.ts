export enum ColorEnum {
  BLUE = 'Blue',
  ORANGE = 'Orange',
  YELLOW = 'Yellow',
  GREEN = 'Green',
  PINK = 'Pink',
  PURPLE = 'Purple',
  RED = 'Red',
  STEEL = 'Steel',
}