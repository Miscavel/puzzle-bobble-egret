export enum BUBBLE_BOARD_EVENT {
  HIT_LAST_ROW = 'hit_last_row',
  CLEAR_ALL_BUBBLES = 'clear_all_bubbles',
  BOARD_IN_DANGER = 'board_in_danger',
  BOARD_IN_SAFETY = 'board_in_safety',
}