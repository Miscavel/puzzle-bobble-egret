export enum SAFETY_LINE_STATE {
  SAFE = 'safe',
  DANGER = 'danger',
}