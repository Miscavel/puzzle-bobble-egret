import { STAGE_EVENT } from './Library/Enum/Stage';
import { SCENE_KEY } from './Enum/Scene';
import SceneManager from './Library/Manager/SceneManager';
import PreloadScene from './Scene/PreloadScene';
import GameplayScene from './Scene/GameplayScene';
import AssetManager from './Library/Manager/AssetManager';
import AnimationManager from './Library/Manager/AnimationManager';

class Main extends egret.DisplayObjectContainer {
    private sceneManager = new SceneManager(this);

    private assetManager = new AssetManager();

    private animationManager = new AnimationManager();

    public constructor() {
        super();
        this.setupScenes();
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAddToStage, this);
    }

    private setupScenes() {
        this.sceneManager.registerScene(new PreloadScene(this.assetManager, this.animationManager));
        this.sceneManager.registerScene(new GameplayScene(this.assetManager, this.animationManager));
    }

    private onAddToStage(event: egret.Event) {
        egret.lifecycle.addLifecycleListener((context) => {
            // custom lifecycle plugin
            context.onUpdate = () => {
                this.stage.dispatchEvent(new egret.Event(STAGE_EVENT.UPDATE));
            }
        });

        egret.lifecycle.onPause = () => {
            egret.ticker.pause();
        }

        egret.lifecycle.onResume = () => {
            egret.ticker.resume();
        }

        this.stage.addEventListener(STAGE_EVENT.GO_TO_SCENE, (eventData: egret.Event) => {
            const { sceneKey } = eventData.data;
            this.sceneManager.start(sceneKey as SCENE_KEY);
        }, this);
        this.stage.dispatchEvent(new egret.Event(STAGE_EVENT.GO_TO_SCENE, false, false, { sceneKey: SCENE_KEY.PRELOAD }));
    }
}